<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\Car;

class CarController extends Controller
{
    //

    public function index(Request $request){

            $cars = Car::all()->load('user');
            $data = [
                'cars' => $cars,
                'status' => 200,
                'status' => 'success'
            ];


        return response()->json($data, 200);
    }

    public function store(Request $request) {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //Recoger datos POST
            $json = $request->input('json');
            $params = json_decode($json);
            $params_array = json_decode($json, true);

            $validate = \Validator::make($params_array, [
                'title' => 'required|min:5',
                'description' => 'required',
                'price' => 'required',
                'status' => 'required'
            ]);


            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            //Usuario autenticado
            $user = $jwtAuth->checkToken($hash, true);

            //Guardar coche
            $car = new Car();
            $car->user_id = $user->sub;
            $car->title = $params->title;
            $car->description = $params->description;
            $car->price = $params->price;
            $car->status = $params->status;

            $car->save();

            $data = [
                'car' => $car,
                'status' => 'success',
                'code' => 200
            ];

        } else {
            $data = [
                'message' => 'Login incorrecto',
                'status' => 'error',
                'code' => 500
            ];
        }

        return response()->json($data, 200);
    }

    public function show($id) {
        $car = Car::find($id);
        if (!is_object($car)) {
            return response()->json([
                'status' => 'error',
                'code' => 400,
                'message' => 'El registro no existe'
            ]);
        }

        $car->load('user');

        return response()->json([
           'car' => $car,
           'status' => 'success'
        ], 200);
    }

    public function update($id, Request $request) {

        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //Recoger datos POST
            $json = $request->input('json');
            $params = json_decode($json);
            $params_array = json_decode($json, true);

            $validate = \Validator::make($params_array, [
                'title' => 'required|min:5',
                'description' => 'required',
                'price' => 'required',
                'status' => 'required'
            ]);

            unset($params_array['id']);
            unset($params_array['user']);
            unset($params_array['user_id']);
            unset($params_array['created_at']);

            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            //Usuario autenticado
            $user = $jwtAuth->checkToken($hash, true);

            //Actualizar coche
            $car = Car::where('id', $id)->update($params_array);


            $data = [
                'car' => $params,
                'status' => 'success',
                'code' => 200
            ];

        } else {
            $data = [
                'message' => 'Login incorrecto',
                'status' => 'error',
                'code' => 500
            ];
        }

        return response()->json($data, 200);
    }

    public function destroy($id, Request $request) {
        $hash = $request->header('Authorization', null);

        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            //Recoger datos POST

            $car = Car::find($id);
            if ($car) {
                $car->delete();
                $data = [
                    'car' => $car,
                    'status' => 'success',
                    'code' => 200
                ];
            } else {
                $data = [
                    'message' => 'No se encontro el registro',
                    'status' => 'error',
                    'code' => 400
                ];
            }

        } else {
            $data = [
                'message' => 'Login incorrecto',
                'status' => 'error',
                'code' => 500
            ];
        }

        return response()->json($data, 200);
    }
}
