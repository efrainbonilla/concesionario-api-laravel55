<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;
use Mockery\Exception;

class JwtAuth {

    public $key;

    public function __construct()
    {
        $this->key = 'secret-key3123';
    }

    public function signup($email, $password, $getToken = null) {

        $user = User::where([
           'email' => $email,
           'password' => $password
        ])->first();


        if (is_object($user)) {
            //generar token
            $token = [
                'sub' => $user->id,
                'email' => $user->email,
                'name' => $user->name,
                'surname' => $user->surname,
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60) //una semana 7 dias, 24 horas, 60 minutos, 60 segundos
            ];

            $jwt = JWT::encode($token, $this->key, 'HS256');

            if (is_null($getToken)) {
                return $jwt;
            } else {
                $decoded = JWT::decode($jwt, $this->key, ['HS256']);
                return $decoded;
            }

        } else {
            //return error
            return [
                'status' => 'error',
                'code' => 500,
                'message' => 'Login ha fallado'
            ];
        }
    }

    public function checkToken($jwt, $getIdentity = false) {
        $auth = false;
        $decoded = null;
        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }

        if (is_object($decoded) && isset($decoded->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }

        if ($getIdentity) {
            return $decoded;
        }

        return $auth;
    }
}