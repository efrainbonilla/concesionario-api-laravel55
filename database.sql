CREATE TABLE IF NOT EXISTS users
(
  id SERIAL NOT NULL CONSTRAINT pk_users PRIMARY KEY,
  role VARCHAR(20),
  name VARCHAR(255),
  surname VARCHAR(255),
  password VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  remember_token VARCHAR(255)
);


CREATE TABLE IF NOT EXISTS cars
(
  id SERIAL NOT NULL CONSTRAINT pk_cars PRIMARY KEY,
  user_id INTEGER,
  title VARCHAR(255),
  description TEXT,
  price VARCHAR(30),
  status VARCHAR(30),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);



ALTER TABLE cars ADD CONSTRAINT fk_cars_user FOREIGN KEY (user_id) REFERENCES users;